/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const bcrypt = require("bcrypt");
const Promise = require("bluebird");
const passportS = require("./services/passport");

const userList = require("./services/user-list");

const bcryptHash = Promise.promisify(bcrypt.hash);
const bcryptGenSalt = Promise.promisify(bcrypt.genSalt);

const PACKAGE = require("../package.json");
const DEFAULT_CONFIG = require("./constants/default-config");

/*===================================================== Exports  =====================================================*/

module.exports = Plugin;

/*==================================================== Functions  ====================================================*/

function Plugin(renato, config) {
  this.renato = renato;
  this.config = _.defaultsDeep({}, config, DEFAULT_CONFIG);
  this.middleware = Plugin.middleware;
  this.log = renato.log.child({plugin: {name: PACKAGE.name, version: PACKAGE.version}});
  this.passport = null;
  this.users = null;
}

Plugin.prototype.init = function () {
  return userList
      .read(this)
      .then((userData) => {
        this.users = userData;
        this.passport = passportS.init(this);
      });
};

Plugin.prototype.renderPre = function (data) {
  data.locals.user = data.req.user;
  return data;
};

Plugin.prototype.bindRoutes = function (routers) {
  let initialize = this.passport.initialize();
  let session = this.passport.session();
  if (routers.ui != null) {
    routers.ui.use(initialize);
    routers.ui.use(session);
  }
  if (routers.api != null) {
    routers.api.use(initialize);
    routers.api.use(session);
  }
};

Plugin.prototype.bindRoutesPost = function (routers) {
  if (routers.ui != null) {
    routers.ui.post("/login", this.login({failureRedirect: "/login", successRedirect: "/"}));
    routers.ui.get("/logout", this.logout({successRedirect: "/login"}));
  }
  if (routers.api != null) {
    routers.api.post("/login", this.login({failureMessage: true, successMessage: true}));
    routers.api.post("/logout", this.logout({successMessage: true}));
  }
};

Plugin.prototype.login = function (options) { return this.passport.authenticate("local", options); };

Plugin.prototype.logout = function (options) {
  return function (req, res) {
    req.session.destroy();
    req.logout();
    if (options.successMessage) {
      res.status(200).send();
    } else if (typeof options.successRedirect === "string") {
      res.redirect(302, options.successRedirect);
    }
  };
};

Plugin.prototype.persist = function () { return userList.persist(this); };

// caution: persist is needed to be called afterwards
Plugin.prototype.addUser = function (username, password, roles, data) {
  if (data == null) { data = {}; } else { data = _.cloneDeep(data); }
  data.id = ++this.users.maxId;
  data.roles = roles;
  data.role = {};
  _.each(roles, (role) => data.role[role] = 1);
  data.username = username;
  return bcryptGenSalt(this.config.saltRounds)
      .then((salt) => bcryptHash(password, salt))
      .then((hash) => data.password = hash)
      .then(() => {
        this.users.byId[data.id.toString()] = this.users.byUsername[data.username] = data;
        this.users.list.push(data);
        return data;
      });
};

// caution: persist is needed to be called afterwards
Plugin.prototype.removeUser = function (id) {
  let user = this.users.byId[id && id.toString()];
  if (user == null) { return false; }
  let idx = _.indexOf(this.users.list, user);
  this.users.list.splice(idx, 1);
  Reflect.deleteProperty(this.users.byId, user.id.toString());
  Reflect.deleteProperty(this.users.byUsername, user.username);
  return true;
};

Plugin.middleware = {

  guest() {
    return function (req, res, next) {
      if (req.isAuthenticated()) { return res.status(403).send(); }
      next();
    };
  },

  role(roleString) {
    return function (req, res, next) {
      if (req.isUnauthenticated()) {
        if (roleString === null) { return next(); }
        return res.status(401).send();
      }
      if (!_.includes(req.user.roles, roleString)) { return res.status(403).send(); }
      next();
    };
  },

  roles(rolesList) {
    return function (req, res, next) {
      if (req.isUnauthenticated()) {
        if (_.includes(rolesList, null)) { return next(); }
        return res.status(401).send();
      }
      if (!_.some(req.user.roles, matchesAnyRole)) { return res.status(403).send(); }
      next();
    };

    function matchesAnyRole(roleString) { return _.includes(rolesList, roleString); }
  }

};
