/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const Promise = require("bluebird");
const bcrypt = require("bcrypt");

const bcryptHash = Promise.promisify(bcrypt.hash);
const bcryptGenSalt = Promise.promisify(bcrypt.genSalt);

const PACKAGE = require("../../package.json"), MODULE = PACKAGE.name;
const PERSIST_FIELDS = ["id", "username", "password", "roles", "data"];
const KEY = "users.json";

/*===================================================== Exports  =====================================================*/

exports.read = read;
exports.persist = persist;

/*==================================================== Functions  ====================================================*/

// read

function read(authPlugin) {
  return authPlugin.renato
      .hookReduce("storage::read", {module: MODULE, key: KEY})
      .then((result) => {
        if (result.value === void 0) { return writeDefault(authPlugin); }
        return postReadProcess(JSON.parse(result.value));
      });
}

function writeDefault(authPlugin) {
  authPlugin.log.verbose("create default user-list");
  let users = {list: _.cloneDeep(require("../constants/default-user-list"))};
  users.maxId = _.last(users.list).id;
  return Promise
      .map(users.list, _.partial(prepareDefaultUser, authPlugin))
      .then(() => {
        return authPlugin.renato
            .hookReduce("storage::write", {module: MODULE, key: KEY, value: JSON.stringify(users)});
      })
      .then(() => authPlugin.log.info("user-list creation succeeded"))
      .then(_.partial(postReadProcess, users));
}

function prepareDefaultUser(authPlugin, user) {
  authPlugin.log.info("create default user", user);
  return bcryptGenSalt(authPlugin.config.saltRounds)
      .then((salt) => bcryptHash(user.password, salt))
      .then((hash) => user.password = hash);
}

function postReadProcess(users) {
  let list = users.list, user;
  let byId = users.byId = {}, byUsername = users.byUsername = {};
  for (let i = 0; i < list.length; i++) {
    user = list[i];
    user.role = {};
    _.each(user.roles, assignRole);
    byId[user.id.toString()] = byUsername[user.username] = user;
  }
  return users;

  function assignRole(role) { user.role[role] = 1; }
}

// write

function persist(authPlugin) {
  let users = {list: _.map(authPlugin.users.list, preparePersistUser), maxId: authPlugin.users.maxId};
  let log = authPlugin.log.child({users: {amount: users.list.length, maxId: users.maxId}, cmdTime: Date.now()});
  log.debug("write user-list");
  return authPlugin.renato
      .hookReduce("storage::write", {module: MODULE, key: KEY, value: JSON.stringify(users)})
      .then(() => log.debug("user-list written"));
}

function preparePersistUser(user) { return _.pick(user, PERSIST_FIELDS); }
