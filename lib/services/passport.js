/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const bcrypt = require("bcrypt");
const Promise = require("bluebird");
const Passport = require("passport").Passport;
const LocalStrategy = require("passport-local");

const bcryptCompare = Promise.promisify(bcrypt.compare);

/*===================================================== Exports  =====================================================*/

exports.init = init;

/*==================================================== Functions  ====================================================*/

function init(plugin) {
  const passport = new Passport();
  passport.use(genStrategy(plugin));
  passport.serializeUser((user, done) => done(null, user.id));
  passport.deserializeUser((id, done) => done(null, plugin.users.byId[id]));
  return passport;
}

function genStrategy(plugin) {
  return new LocalStrategy(
      (username, password, done) => {
        const users = plugin.users;
        if (users == null) { return done(new Error("Users have not been initialized yet.")); }
        findUser(users, username, password)
            .then(_.partial(done, null), done);
      }
  );
}

function findUser(users, username, password) {
  let user = users.byUsername[username];
  if (user == null) { return Promise.resolve(false); }
  return bcryptCompare(password, user.password)
      .then((bool) => bool ? user : false);
}
